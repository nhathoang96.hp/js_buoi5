function sayHello(){
    console.log("Chào chị");
    console.log("Have a good day");
}
// thực thi
sayHello();
sayHello();
sayHello();
sayHello();
sayHello();

function sayHelloWithName(username){
    // username:param không cần khai báo var
    console.log("Chào", username);
}

sayHelloWithName("Alice"); 
sayHelloWithName("Bob")

function tinhDTB(toan, ly, hoa, username) {
    var dtb=(toan+ly+hoa)/3;
    console.log("Chào", username);
    console.log(`Điểm trung bình của ${username} là`);
    console.log("dtb: ", dtb);
    // chỉ return về 1 giá trị
    return dtb;
    console.log("after return"); 
    // sau return không thực thi
}

var sv1=tinhDTB(4,5,6,"alice");
console.log('sv1: ', sv1);
var sv2=tinhDTB(9,9,9, "bob");
console.log('sv2: ', sv2);

